import * as React from "react";
import { Counter } from "./Counter/Counter";

import "./AppPage.css";
import logo from "./assets/logo.svg";

export class AppPage extends React.Component {
  public render() {
    return (
      <div className="AppPage">
        <header className="AppPage-header bg-purple-darker mb-6">
          <div className="container">
            <img src={logo} className="AppPage-logo" alt="logo" />
            <h1 className="AppPage-title">Welcome to React</h1>
          </div>
        </header>
        <div className="container">
          <div>
            <p className="AppPage-intro">
              To get started, edit <code>src/App.tsx</code> and save to reload.
            </p>
          </div>
          <div className="mt-8">
            <Counter />
          </div>
        </div>
      </div>
    );
  }
}
