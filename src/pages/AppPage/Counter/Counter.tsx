import * as React from "react";
import { connect } from "react-redux";
import { increment, decrement } from "../../../actions";

interface ICounterProps {
  counter: number;
  edit: any;
}

const mapStateToProps = (state: any) => {
  return { counter: state.counter.n };
};

const mapDispatchToProps = (dispatch: any, ownProps: any) => {
  return {
    edit: (type: "++" | "--") => {
      return () => {
        if (type === "++") dispatch(increment());
        else if (type === "--") dispatch(decrement());
      };
    }
  };
};

class ConnectedCounter extends React.Component<ICounterProps> {
  public render() {
    return (
      <div className="flex justify-center items-center">
        <button
          className="bg-purple-darker hover:bg-purple-dark w-10 text-white font-bold py-2 px-4 mx-4 rounded-full outline-none"
          onClick={this.props.edit("--")}
        >
          -
        </button>
        <div className="text-xl font-medium">{this.props.counter}</div>
        <button
          className="bg-purple-darker hover:bg-purple-dark w-10 text-white font-bold py-2 px-4 mx-4 rounded-full outline-none"
          onClick={this.props.edit("++")}
        >
          +
        </button>
      </div>
    );
  }
}

export const Counter = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedCounter);
