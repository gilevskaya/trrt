export const increment = () => ({
  type: "SUM_COUNT",
  addition: 1
});

export const decrement = () => ({
  type: "SUM_COUNT",
  addition: -1
});
