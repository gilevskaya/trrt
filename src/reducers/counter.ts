export interface IReduxCounterState {
  n: number;
}

export const initialState = {
  n: 0
};

export const counter = (
  state: IReduxCounterState = initialState,
  action: any
) => {
  switch (action.type) {
    case "SUM_COUNT":
      return { n: state.n + action.addition };
    default:
      return state;
  }
};
