import { combineReducers } from "redux";
import { counter } from "./counter";
export { IReduxCounterState } from "./counter";

export const rootReducer = combineReducers({ counter });
